using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitWithBtn : MonoBehaviour
{
    [SerializeField]
    private KeyCode Button;
    CharacterController controller;
    Vector3 name;
    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(Button))
        {
            Debug.Log("Game Quit.");
            Application.Quit();

            controller.Move(name * Time.deltaTime);
        }
    }
}
