using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour
{

    public bool stopwatchActive = false;

   [SerializeField] bool _forTesting;
    [SerializeField] float _currentTime;
    //[SerializeField] int _startTime;

    [SerializeField] Text _currentTimeText;
    // Start is called before the first frame update
    void Start()
    {
        _currentTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
     

        if (Input.GetKeyDown(KeyCode.Space) && _forTesting == true)
        {
            StopStopwatch();
            _forTesting = false;
        }

        else if (Input.GetKeyDown(KeyCode.O) && _forTesting == false)
        {
            StartStopwatch();
            _forTesting = true;
        }

        if (stopwatchActive == true)
        {
            _currentTime = _currentTime + Time.deltaTime;

            

        }
           TimeSpan time = TimeSpan.FromSeconds(_currentTime);
        _currentTimeText.text = time.ToString(@"mm\:ss\:ff");

        

         

    }

       

        public void StartStopwatch()
        {
            stopwatchActive = true;
        }

         public void StopStopwatch()
        {
            stopwatchActive = false;
        }
    }

