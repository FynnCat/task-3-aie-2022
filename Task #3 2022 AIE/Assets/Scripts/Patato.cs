using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patato : MonoBehaviour
{

    [SerializeField] float speed = 20f;
    public Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.forward * speed;
    }

   void OnTriggerEnter (Collider other)
   {
       Debug.Log(other.name);
       Destroy(gameObject);
   }
}
