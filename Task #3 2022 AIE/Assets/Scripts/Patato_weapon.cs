using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class Patato_weapon : MonoBehaviour
{
    //reference to private InputActionAsset
    [SerializeField] InputActionAsset _playercontols;

    //Private reference to Input action move, found in playercontrols var above.
    InputAction _fire;
    [SerializeField] Transform _firePoint;

    [SerializeField] GameObject _patato;

    [SerializeField] float _shootForce, _upwardForce;

    [SerializeField] float _timebetweenshooting, _spread, _reloadTime, _timebetweenshots;

    [SerializeField] int _bulletsLeft, _bulletsShot, _magazineSize, _bulletsPerTap;

    [SerializeField] bool _shooting, _readyToShoot, reloading, _allowButtonHold, _reloading;

    [SerializeField] Camera _fpsCamera;

    [SerializeField] Transform _attackPoint;

    [SerializeField] bool _allowInvoke = true;

    [SerializeField] MouseLook _mouselook;

    [SerializeField] TextMeshProUGUI _ammunitionDisplay;

    void Awake()
    {

        _bulletsLeft = _magazineSize;
        _readyToShoot = true;
    }

    
    // Start is called before the first frame update
    void Start()
    {
        var _gameplayActionMap = _playercontols.FindActionMap("Player");


        //Sets the move inputaction.
        _fire = _gameplayActionMap.FindAction("Fire");

        //Checks to perform, canceled inputs.
        _fire.performed += OnAttack;
        _fire.canceled += OnAttack;

        //Turns move to Enabled. This is just here as a precaution.
        _fire.Enable();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            shooting();
        }

        if (_ammunitionDisplay != null)
        {
            _ammunitionDisplay.SetText(_bulletsLeft / _bulletsPerTap + "/" + _magazineSize / _bulletsPerTap);
        }
    }

    private void MyInput()
    {
        if (_allowButtonHold) _shooting = Input.GetKeyDown(KeyCode.Mouse0);
        else _shooting = Input.GetKeyDown(KeyCode.Mouse0);

        if (Input.GetKeyDown(KeyCode.Mouse0) && _bulletsLeft < _magazineSize && !reloading)
        {
            Reload();
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && _readyToShoot && _shooting && !reloading && _bulletsLeft <= 0)
        {
            Reload();
        }

        //shooting.
        if (Input.GetKeyDown(KeyCode.Mouse0) && _readyToShoot && _shooting && !_reloading && _bulletsLeft > 0)
        {
            _bulletsShot = 0;

            shooting();
        }
    }

    private void shooting()
    {
        Vector3 _targetPoint = _mouselook.pointLook;
        Vector3 _direction = _targetPoint - _attackPoint.position;

        GameObject _currentpatato = Instantiate(_patato, _attackPoint.position, Quaternion.identity);

        _currentpatato.transform.forward = _direction.normalized;


        _currentpatato.GetComponent<Rigidbody>().AddForce(_direction.normalized * _shootForce, ForceMode.Impulse);

        _bulletsLeft--;
        _bulletsShot++; 

        Invoke("ResetShot", _timebetweenshooting);
        _allowInvoke = false;

    }

    private void ResetShot()
    {
        _readyToShoot = true;
        _allowInvoke = true;
    }

    private void Reload()
    {
        _reloading = true;
        Invoke("ReloadFinished", _reloadTime);
    }

    private void ReloadFinished()
    {
      _bulletsLeft = _magazineSize;
      reloading = false;  
    }

    public void OnAttack(InputAction.CallbackContext context)
     {
        if (_bulletsLeft < _magazineSize && !reloading)
        {
            Reload();
        }

        if (_readyToShoot && _shooting && !reloading && _bulletsLeft <= 0)
        {
            Reload();
        }

        //shooting.
        if (_readyToShoot && _shooting && !_reloading && _bulletsLeft > 0)
        {
            _bulletsShot = 0;

            shooting();
        }
    }


}
