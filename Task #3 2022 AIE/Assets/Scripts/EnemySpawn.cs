using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{

    //Gameobject.

    //Refrence to the enemy object.
    [SerializeField] GameObject _enemy;

    //Array.

    //Creating array & setting it's length.
    [SerializeField] Transform[] _spawnlocations = new Transform[2];

    //Float.

    //Creating _enemyspawned float.
    [SerializeField] float _enemiesSpawned;

    //Ints.

    //Creating _numberofenemies int.
    [SerializeField] int _numberofenemies;

    //Creating _time int & setting it to 5.
    [SerializeField] int _time = 5;

    //Bools.

    //Creating _timeAtthree bool.
    [SerializeField] bool _timeAtThree;

    //Creating _hasreachednumber bool.
    public bool _hasreachednumber;

    
    // Start is called before the first frame update
    void Start()
    {
        //Setting _numberofenemies bool to 10.

        _numberofenemies = 10;

        //Spawning the enemy prefab at a randomly picked transform.

        Instantiate(_enemy, _spawnlocations[Random.Range(0, _spawnlocations.Length)].position, Quaternion.identity);

        //Setting _enemies to 1.

        _enemiesSpawned = 1;

        //Starting WaitTime().

        StartCoroutine(WaitTime());
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //WaitTime IEnumerator used for a timed delay.

    IEnumerator WaitTime()
    {
        //Deg.Log on start of this IEnumerator. "Waiting... For spawn."

        Debug.Log("Waiting... For spawn.");

        //Waits for _time amount of seconds.

        yield return new WaitForSeconds(_time);

    //Checks if the amount of enimes spawned equal the amount of enemies before next change.

if (_enemiesSpawned <= _numberofenemies)

        //If it is...

        {
            _hasreachednumber = false;
            //Spawning the enemy prefab at a randomly picked transform.

            Instantiate(_enemy, _spawnlocations[Random.Range(0, _spawnlocations.Length)].position, Quaternion.identity);

            //Sets _enemiesSpawned to _enemiesSpawned + 1.
            _enemiesSpawned++;

            //Debug.Log at the end of the if statement. "Spawned".

            Debug.Log("Spawned.");

             //Debug.Log at the end of the if statement. "End of WaitTime".

            Debug.Log("End of WaitTime.");

            //Starting WaitTime().

            StartCoroutine(WaitTime());
        }
        //If the last if statment is false this will check if _timeAtThree is set to false. 
        else if (_timeAtThree == true)

        //If it is...

        {
            //sets _time to 3.

            _time = 3;

            //Sets _numberofeneimes to _numberofeneies + 3;

            _numberofenemies = _numberofenemies + 3;

            //Sets _enemiesSpawned to 0;
            _enemiesSpawned = 0;

            //Debug.Log at the end of the if statement. "End of WaitTime".

            Debug.Log("End of WaitTime.");

        //Sets _timeAtThree to true.

        _timeAtThree = true;
        
        //Starting WaitTime().

        StartCoroutine(WaitTime());
        }

        //If both the last two if statements fail then do this.

        else
        {
            //Sets _numberofeneimes to _numberofeneies + 3;

            //_numberofenemies = _numberofenemies + 3;

            //Sets _enemiesSpawned to 0;
            _enemiesSpawned = 0;

            _hasreachednumber = true;

            //Debug.Log at the end of the if statement. "End of WaitTime".

            Debug.Log("End of WaitTime.");

        //Starting WaitTime().

        StartCoroutine(WaitTime());
        
        }

        

    }
}
