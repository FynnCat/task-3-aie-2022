using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MouseLook : MonoBehaviour
{ 


    
   
//Script by gamesplusjames
   
//Varibles

//Camera
[SerializeField] private Camera cam; //private, but can be changed in inspector.

//Ray
Ray cameraRay;

//Plane (this plane is not visible)
 Plane groundPlane = new Plane(Vector3.up, Vector3.zero);

 //float
float rayLength; //this stores the length of the ray.

//creates vector3 used to store mouse position.
public Vector3 pointLook;
    void Update()
    {
       
        //This creates a line from the camera to the mouse position o the screen.
         cameraRay = cam.ScreenPointToRay(Input.mousePosition);

        //If raycast hits something else in the world, set raylength.
        if (groundPlane.Raycast(cameraRay, out rayLength))
        {

            //Set the point that the player is going to look at.
            pointLook = cameraRay.GetPoint(rayLength);

            //Draws a line in scene view to show where the mouse position is.
            Debug.DrawLine(cameraRay.origin, pointLook, Color.blue);

            //has the player look at mouse point.
            transform.LookAt(new Vector3(pointLook.x, transform.position.y, pointLook.z));
        }
    }

}
