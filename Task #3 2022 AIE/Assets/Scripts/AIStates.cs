using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

//Script by Fynn Burgess.

public class AIStates : MonoBehaviour {

    //GameObject's.

    //Creates _player var.
    [SerializeField] GameObject _player;

      //Creates _this var.
    [SerializeField] GameObject _this;


    //Health.

    //Creates _playerhealth var.
    [SerializeField] Health _playerhealth;

    //Creates _zombiehealthScript var.
    [SerializeField] Health _zombiehealthScript;

    //NavMeshAgent.

    //Creates _agent var.
    [SerializeField] NavMeshAgent _agent; 

    //SphereCollider.

    //Creates _attackdistance var.
    [SerializeField] SphereCollider _attackdistance;

    //Bool's.

    //Creates _isindistantence bool.
    [SerializeField] bool _isindistantence = false;

    //Creates _isindistantence bool.
    [SerializeField] bool _hasentered = false;

    //Creates _isindistantence bool.
    [SerializeField] bool _hasexited = false;

    //Creates _hasAddedKill bool.
    [SerializeField] bool _hasAddedKill = false;

    //Creates _hasKilled bool.
    [SerializeField] bool _hasKilled = false;

    //Creates _zombieAnimator var.
    [SerializeField] Animator _zombieAnimator;

   //Float.

    //Creates _isindistantence var.
    [SerializeField] float _health;

    //EnemySpawn.

    //Creates _isindistantence var.
    [SerializeField] EnemySpawn _enemySpawn;

    //Multipler.

    //Creates Multiplier var "_multiplier".
    [SerializeField] Multiplier _multiplier;

    //Int.

    //Creates _damage int.
    [SerializeField] int _damage = 11;

    //CapsuleCollider.

    //Creates _mainColloider CapsuleCollider
    [SerializeField] CapsuleCollider _mainColloider;

    
    //Plays only when the game starts.
    void Start()
    {
        //Sets animator bool "IsWalking" to true.
        _zombieAnimator.SetBool("IsWalking", true);

        //Get's the gameObject called with the tag "Player" & sets it to _player.
        _player = GameObject.FindGameObjectWithTag("Player");

        //Get's the gameObject of type "EnemySpawn" & sets it to _enemySpawn.
        _enemySpawn = GameObject.FindObjectOfType<EnemySpawn>();

        //Get's the gameObject of type "Multiplier" & sets it to _multiplier.
        _multiplier = GameObject.FindObjectOfType<Multiplier>();

        //Get's the Component of type "Health" & sets it to _playerhealth.
        _playerhealth = _player.GetComponent<Health>();

        SetupAI();
    }
	// Update is called once per frame
	void Update () 
    {
        
         SetupAI();

            //Calls the function "die".
            die();
            
        
         //Sets animator bool "IsWalking" to true.
        _zombieAnimator.SetBool("IsWalking", true);

        

        //Update's the enemey's health.
        _health = _zombiehealthScript._health;

       
        
	}

    //Plays 50 times a second.
    void FixedUpdate()
    {

        //Checks if the player's health is less then or equal to 0.
        if (_playerhealth._health <= 0)
        {
            
            //Sets animetor bool "IsDead" to true.
            _playerhealth.animator.SetBool("IsDead", true);
            
            //Sets the end panel to active.
            _playerhealth.end.SetActive(true);
        }
        
        //checks if the "_hasreachednumber" bool on "enemySpawn" is true.
        if (_enemySpawn._hasreachednumber == true)
        {

            //Sets current damage + 4.
            _damage = _damage + 4;

            //Debug.Log displays enemy's damage. 
            Debug.Log ("Enemy's damage: " + _damage);

            //Calls "WaitFrame.
            StartCoroutine(WaitFrame());
        }
    }

    //This is a custom function that deals whith damage.
    void Attack()
    {
        //Player takes damage.
        _playerhealth._health = _playerhealth._health - _damage;
        
    }

    //Function called when something walks into trigger.
  private void OnTriggerEnter(Collider other)
    {

        //Checks if other.tag equals "Player" && "_hasentered" equals false
        if (other.tag == "Player" && _hasentered == false)
        {
            //Sets "_hasentered" bool equal to true.
            _hasentered = true;

            //Debug.Log "Player is in attack range!".
            Debug.Log("Player is in attack range!");
            //Sets "_isindistantence equal to true".
            _isindistantence = true;

            //Sets animetor bool "IsAttacking" to true.
            _zombieAnimator.SetBool("IsAttacking", true);

            //Sets "_hasexited" bool equal to false.
            _hasexited = false;
            Attack();
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && _hasexited == false)
        {
            //Sets "_hasexited" bool equal to true.
            _hasexited = true;

            //Sets "_isindistantence" bool equal to false.
            _isindistantence = false;

            //Debug.Log "Player is in attack range!".
            Debug.Log("Player left attack range!");

            Debug.Log("Is player in attack range? " + _isindistantence);

            //Sets animetor bool "IsAttacking" to false.
            _zombieAnimator.SetBool("IsAttacking", false);

            //Sets "_hasentered" bool equal to false.
            _hasentered = false;
        }


    }

    //Function to deal with death & the death animation.
    private IEnumerator WaitTimeDeath()
    {

        //Checks if enemys health is less then 0.
        if (_health < 0)
        {

            //Debug.Log "Dying".
            Debug.Log("Dying");
            _damage = 0;
            
            
            //Sets animetor bool "IsDead" to true.
            _zombieAnimator.SetBool("IsDead", true);

            //Debug.Log "Waiting...".
            Debug.Log("Waiting...");
            yield return new WaitForSeconds(6);
            kills();

            //Debug.Log "Enemy Killed!".
            Debug.Log("Enemy killed!");
        }

        //Debug.Log "Waiting...".
        Debug.Log("Waiting...");
        yield return new WaitForSeconds(1);
        if(_isindistantence == true)
        {

            //Debug.Log displays if the player is in attack range.
            Debug.Log("Is player in attack range? " + _isindistantence);
            Attack();
        }

        //Else.
        else
        {
            //Debug.Log displays if the player is in attack range.
            Debug.Log("Is player in attack range? " + _isindistantence);
        }

    }

    //Handles the start of the death process.
    public void die()
    {
        _hasKilled = true;
        //Checks if enemy health is less then 0.
        if (_health < 0)
        {

            //Debug.Log "Dying".
            Debug.Log("Dying");
             _agent.height = 0;
            _mainColloider.enabled = false;
            
      // yield return new WaitForSeconds(2);
          StartCoroutine(WaitTimeDeath());

        }
      
        
    }

    //Custom IEnumerator "WaitFrame".
    IEnumerator WaitFrame()
    {
        //Waits for a frame.
        yield return new WaitForEndOfFrame();

        //Sets "_haseachnumber" on the "EnemySpawn" script to false.
        _enemySpawn._hasreachednumber = false;       

    }

    void kills()
    {
       if (_hasAddedKill == false)
       {
           
           _hasAddedKill = true;
           //Sets "kills" in "_multipler" to current value plus 1.
            _multiplier.kills = _multiplier.kills + 1;
            Destroy(this.gameObject);
       } 
    }

    public void SetupAI()
    {
        //Sets the enemy's walk point.
        _agent.SetDestination(_player.transform.position);
    }


}

