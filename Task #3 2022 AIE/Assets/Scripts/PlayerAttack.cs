using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] GameObject Enemy;

    [SerializeField] Health Enemyhealth;


    [SerializeField] int damage;

    [SerializeField] int _currentWeapon = 0;

    [SerializeField] WeaponSelect _weaponSelect;

    //Multipler.

    //Creates Multiplier var "_multiplier".
    [SerializeField] Multiplier _multiplier;
    
    // Start is called before the first frame update
    void Start()
    {
        _weaponSelect = FindObjectOfType<WeaponSelect>();
        _multiplier = FindObjectOfType<Multiplier>();
        
    }

    // Update is called once per frame
    void Update()
    {
       
        
        _currentWeapon = _weaponSelect.weapon;
    }

     private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Zombie") //Input.GetKeyDown(KeyCode.F))
        {
           Enemy = other.gameObject;
           Enemyhealth = Enemy.GetComponent<Health>();

           
            Attack();
            
        }
}

    private void Attack()
    {
        
        if (_currentWeapon == 0)
        {

             damage = (50 * _multiplier.damageAmount);
            Debug.Log("Damage: " + damage);
            Enemyhealth._health = Enemyhealth._health - (damage);
            Debug.Log("Enemy's heralth: " + Enemyhealth._health);
        }

        else if (_currentWeapon == 1)
        {
            
            damage = (30 * _multiplier.damageAmount);
            Debug.Log("Damage: " + damage);
            Enemyhealth._health = Enemyhealth._health - (damage);
            Debug.Log("Enemy's heralth: " + Enemyhealth._health);
        }

        else if (_currentWeapon == 2)
        {

            damage = (20 * _multiplier.damageAmount);
            Debug.Log("Damage: " + damage);
            Enemyhealth._health = Enemyhealth._health - (damage);
            Debug.Log("Enemy's heralth: " + Enemyhealth._health);
        }
      
        
    }
}
