using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationManager : MonoBehaviour
{

    [SerializeField] Animator _player;


    [SerializeField] PlayerAttack _playerattack;

    [SerializeField] PlayerMovement _playermovement;

    [SerializeField] WeaponSelect _weaponSelect;

    [SerializeField] Health _health;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (_playermovement.isRunningForwards == true)
        {
            _player.SetBool("WalkingForwards", true);
        }

        if (_playermovement.isRunningBackwards == true)
        {
            _player.SetBool("WalkingBackwards", true);
        }

        if (_playermovement.isRunningSidewardsLeft == true)
        {
           _player.SetBool("WalkingSidewardsLeft", true); 
        }

        if (_playermovement.isRunningSidewardsRight == true)
        {
            _player.SetBool("WalkingSidewardsRight", true);
        }

         if (_playermovement.isRunningForwards == false)
        {
            _player.SetBool("WalkingForwards", false);
        }

        if (_playermovement.isRunningBackwards == false)
        {
            _player.SetBool("WalkingBackwards", false);
        }

        if (_playermovement.isRunningSidewardsLeft == false)
        {
           _player.SetBool("WalkingSidewardsLeft", false); 
        }

        if (_playermovement.isRunningSidewardsRight == false)
        {
            _player.SetBool("WalkingSidewardsRight", false);
        }

        if (_health._health <= 0)
        {
            _player.SetBool("IsDead", true);
        }

    }

    /*

        _cannon animation controller bool names

        WalkingForwards

        WalkingBackwards

        WalkingSidewardsLeft

        WalkingSidewardsRight

        IsDead
    */
}
