using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//Script by Fynn Burgess.
public class Multiplier : MonoBehaviour
{

    //Int

    //Creates int "Kills".
    public int kills;
    //Creates int "currentCombo".
    public int currentCombo;

    //Creates "damageAmount" int.
    public int damageAmount;

    //Text.

    //Creats "_comboDisplay" Text.
    [SerializeField] TextMeshProUGUI _comboDisplay;

    //PlayerAttack.

    //Creates WeaponSrelect var _weaponSelect.
    [SerializeField] WeaponSelect _weaponSelect;

    // Start is called before the first frame update
    void Start()
    {

        //Sets "kills" equal to 0.
        kills = 0;

        //Sets "_currentCombo" equal to 1.
        currentCombo = 1;

        //Displays the number of combos.
            _comboDisplay.text = "Current combo: " + currentCombo.ToString();
    }

    // Update is called once per frame
    void Update()
    {

        _comboDisplay.text = "Current combo: " + currentCombo.ToString();

        //Starts the switch statement that will reference the weapon int in "_weaponSelect".
        switch(_weaponSelect.weapon)
        {

            //If int "weapon" equals 0.
            case 0:

            //Sets "damageAmount" to (50 * "currentCombo").
            damageAmount = (50 * currentCombo);
            break;

            //If int "weapon" equals 1.
            case 1:

            //Sets "damageAmount" to (20 * "currentCombo").
            damageAmount = (20 * currentCombo);

            //End of switch statement.
            break;
        }
        //Checks if "kills is equal to 10.
        if (kills == 10)
        {
            //Sets "_currentCombo" to "_currentCombo" plus 1.
            currentCombo += 1;

            //Displays the number of combos.
            _comboDisplay.text = "Current combo: " + currentCombo.ToString();
            //Sets "kills" equal to 0;
            kills = 0;
        }
    }
}
