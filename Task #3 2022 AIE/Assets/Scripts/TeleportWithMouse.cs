using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//Script by Fynn Burgess.

public class TeleportWithMouse : MonoBehaviour
{

    //Varibles.

    //MouseLook.

    //Creates mouseposition var.
[SerializeField] MouseLook _mouseposition;

    //Vector3.

    //Player location (from).
   [SerializeField] Vector3 _from;

//Creates location var.
[SerializeField] Vector3 _location;

//Creates teleportlocation var.
Vector3 _teleportlocation;

//InputAction.

//Creates teleport inputaction.
InputAction _teleport;

//GameObject.

//Creates player GameObject.
[SerializeField] GameObject _player;

//CharacterController.

//Private reference to private character contoller.
[SerializeField] CharacterController _controller;

//InputActionAsset.

//Creates playercontrols inputactionasset.
 [SerializeField] InputActionAsset _playercontols;

//Light.

//Creates a Light var called "_light".
 [SerializeField] Light _light;


 

    // Start is called before the first frame update
    void Start()
    {
        //Sets the the location that the player is coming from.

        _from = _player.transform.position;

        //Sets the var teleportActionMap to Teleport, found in playercontrols.
        var _teleportActionMap = _playercontols.FindActionMap("Player");


        //Sets the move inputaction.
        _teleport = _teleportActionMap.FindAction("Teleport");

        //Checks to perform, canceled inputs.
        _teleport.performed += Onteleport;
        _teleport.canceled += Onteleport;

        //Turns move to Enabled. This is just here as a precaution.
        _teleport.Enable();
    }

    // Update is called once per frame
    void Update()
    {
        

        //Sets teleportlocation to pointlook found in the mouseposition var.
       _teleportlocation = _mouseposition.pointLook; 
    }

    //private void that is reference when inputs are performed.
    void Onteleport(InputAction.CallbackContext context)
    {

        //Creates the "_new" Vector3 & sets it to the values of the "_teleportlocation" Vector3.
        Vector3 _new = new Vector3(_teleportlocation.x, _teleportlocation.y, _teleportlocation.z);

        //Sets the "_teleportlocation" Vector3 to the same with the y set to 1.
        _teleportlocation = new Vector3(_new.x, 1, _new.z);

        //Sets location to teleportlocation Vector3.
        _location = _teleportlocation;

        //Turns controller enabled to false.
        _controller.enabled = false;

        //Sets the player's position to the mouse position when they clicked.
        _player.transform.position = _teleportlocation;

        //If player has reached teleportlocation ('set charactercontroller.enabled to true').
        if (_player.transform.position == _location)
        {

            //Turns controller enabled to true.
            _controller.enabled = true;

            //Debug log used for testing during development.
            Debug.Log("Player has successfully teleported & should be able to walk!");

            //Sets "_light" intensity to itself - 0.4.
            _light.intensity = _light.intensity - 0.4f;
        }
    }



    //Private void called when the player stays in a collider.
    private void OnTriggerStay(Collider other)
    {

      //Checks if other's tag equals "Level".
        if (other.gameObject.tag == "Level")
        {

            //Player transform equals the "_from" Vector3.
            _player.transform.position = _from;
        }
    }
}


