using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script by Fynn Burgess.

public class Lights : MonoBehaviour
{

    [SerializeField] 
    Light _light;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LightValue());
    }

    IEnumerator LightValue()
    {
        yield return new WaitForSeconds(3);
        _light.intensity = _light.intensity + 0.5f;
        StartCoroutine(LightValue());
    }
   
}
