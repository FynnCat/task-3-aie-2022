using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{

    //Script by Justin P Barnett.

//Veribles 

//reference to private InputActionAsset
    [SerializeField] InputActionAsset _playercontols;
    [SerializeField] WeaponSelect _weaponselect;

    //Private reference to Input action move, found in playercontrols var above.
    InputAction _move;

    //Private reference to private character contoller.
   [SerializeField] private CharacterController _character;

   //float

   //movement speed.
    float _speed = 10f;

    //Vector3

    //used to translate vector2 into vector3.
    private Vector3 _moveVector;

    //Animator.

    //Refrence to the animator.
    public Animator animator; 

    public bool isRunningForwards;
    public bool isRunningBackwards;
    public bool isRunningSidewardsLeft;
    public bool isRunningSidewardsRight;

    

    // Start is called before the first frame update
    void Start()
    {

        //Sets the var gameplayActionMap to player, found in playercontrols.
       var _gameplayActionMap = _playercontols.FindActionMap("Player");


        //Sets the move inputaction.
        _move = _gameplayActionMap.FindAction("Move");

        //Checks to perform, canceled inputs.
        _move.performed += OnMovementChanged;
        _move.canceled += OnMovementChanged;

        //Turns move to Enabled. This is just here as a precaution.
        _move.Enable();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        this.GetComponent<Animator>().runtimeAnimatorController = _weaponselect.currentanim as RuntimeAnimatorController;
        //Sets the players movement & performs it.
        _character.Move(_moveVector * _speed * Time.fixedDeltaTime);
    }

    //public void that is reference when inputs are performed.
    public void OnMovementChanged(InputAction.CallbackContext context)
    {
        //Sets move direction.
        Vector2 direction = context.ReadValue<Vector2>();
        //Converts it to Vector3.
        _moveVector = new Vector3(direction.x, 0, direction.y);
        isRunningForwards = (direction.y > 0.1f);
        animator.SetBool("WalkingForwards", isRunningForwards);
        isRunningBackwards = (direction.y < -0.1f);
        isRunningSidewardsLeft = (direction.x < -0.1f);
        isRunningSidewardsRight = (direction.x > 0.1f);

    }
}
