using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


//Script by Fynn Burgess.
public class WeaponSelect : MonoBehaviour
{

    //Ints.

    //Creates int "weapon".
    public int weapon;

    //Creates int "time".
    public int time;

    //RuntimeAnimatorController's.

    //Creates RuntimeAnimatorController 
    [SerializeField] RuntimeAnimatorController _anim0;

    //Creates RuntimeAnimatorController "_anim0".
    public RuntimeAnimatorController currentanim;

//Creates RuntimeAnimatorController "_anim1".
[SerializeField] RuntimeAnimatorController _anim1;

//Creates RuntimeAnimatorController "_anim2".
[SerializeField] RuntimeAnimatorController _anim2;

//GameObject's.

//Creates GameObject "_weapon0".
[SerializeField] GameObject _weapon0;

//Creates GameObject "_weapon1".
[SerializeField] GameObject _weapon1;

//Creates GameObject "_weapon2".
[SerializeField] GameObject _weapon2;

    //Creates GameObject "_weapon0Panel".
    [SerializeField] GameObject _weapon0Panel;

    //Creates GameObject "_weapon1Panel".
    [SerializeField] GameObject _weapon1Panel;

    //Creates GameObject "_weapon2Panel".
    [SerializeField] GameObject _weapon2Panel;

    //Bool.

    //Creates bool "Isrunning".
    public bool Isrunning;

    //TextMeshProUGUI.

//Creates TextMeshProUGUI "_AmmoDisplay".
[SerializeField] TextMeshProUGUI _AmmoDisplay;

    // Start is called before the first frame update
    void Start()
    {

        //Sets "Isrunning" bool equal to true.
        Isrunning = true;

        //Calls/starts the "Select" custom void.
        Select();

    }

    //Function to deal with waiting between weapon changes.
    private IEnumerator WaitTime()
    {

        //Wait an amount seconds declared int "time".
        yield return new WaitForSeconds(time);

        //Calls/starts the "Select" custom void.
        Select();
    }

    //Custom void called "Select" that deals with selecting a weapon.
    private void Select()
    {

        //Generates a random number between 0 & 1.
        weapon = Random.Range(1, 1);

        //Starts the switch statement that will reference the weapon int.
        switch(weapon)
        {
            //If int "weapon" equals 0.
            case 0:

                //Sets the weapon 0 gameObjects & panel the true, sets currentanim to anim1 & everything else to false.
               _weapon2.active = false;
               _weapon1.active = false;
               _weapon0.active = true;
                _weapon2Panel.SetActive(false);
                _weapon1Panel.SetActive(false);
                _weapon0Panel.SetActive(true);

                currentanim = _anim0;

                break;
            
            //If int "weapon" equals 1.
             case 1: 
               
               //Sets the weapon 1 gameObjects & panel the true, sets currentanim to anim1 & everything else to false.
               _weapon0.active = false;
               _weapon2.active = false;
               _weapon1.active = true;
                _weapon2Panel.SetActive(false);
                _weapon0Panel.SetActive(false);
                _weapon1Panel.SetActive(true);

                currentanim = _anim1;

                break;

                //If int "weapon" equals 2.
                case 2:

                //Sets the weapon 2 gameObjects & panel the true, sets currentanim to anim1 & everything else to false.

                //Sets the "_AmmoDisplay" text to nothing or "".
                _AmmoDisplay.text = "";
                _weapon0.active = false;
                    _weapon1.active = false;
                    _weapon2.active = true;
                _weapon0Panel.SetActive(false);
                _weapon1Panel.SetActive(false);
                _weapon2Panel.SetActive(true);

                currentanim = _anim2;

                    //End of switch statement.
                    break;
        }

        //Debug.Log shows current weapon.
        Debug.Log("Current weapon: " + weapon);

        //Starts IENumator WaitTime.
        StartCoroutine(WaitTime());
    }
}
