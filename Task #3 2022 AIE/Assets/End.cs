using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class End : MonoBehaviour
{
    public int number;
    public Text _text;
    [SerializeField] Text _text2;
    public StopWatch _StopWatch;
    [SerializeField] int _waitTime;
    

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitTime());
    }

    // Update is called once per frame
    void Update()
    {
        _text2.text = "Your time: " + _StopWatch.timeAlive.ToString();
        _text.text = "Your game will close in: " + number.ToString();
        
        if (Input.anyKey)
        {
            SceneManager.LoadScene(0);
        }
        //StartCoroutine(WaitTime());
    }

    IEnumerator WaitTime()
    {
        
        yield return new WaitForSeconds(_waitTime);
       number--;
        StartCoroutine(WaitTime());
        if (number <= 0)
        {
            Application.Quit();
            Debug.Log("Quit");
        }
            
        
        
    }
}
