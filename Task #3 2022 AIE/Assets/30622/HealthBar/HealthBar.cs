using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    public Slider slider;
    public int health;
    
    [SerializeField] Image _fill;

    public int maxhealth = 100;

    [SerializeField] Gradient _gradient;
    [SerializeField] Health _playerhealth;

    void start()
    {
       
    }
    public void Update()
    {
        health = _playerhealth._health;
        slider.value = health;
        _fill.color = _gradient.Evaluate(slider.normalizedValue);



    }
}
