using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;
using System;
using TMPro;

public class StopWatch : MonoBehaviour
{

    public bool stopwatchActive = false;
    [SerializeField] Health _playerhealth;
    [SerializeField] GameObject _player;


    [SerializeField] bool _forTesting;
    [SerializeField] float _currentTime;
    //[SerializeField] int _startTime;

    public TextMeshProUGUI currentTimeText;
    public float timeAlive;
    // Start is called before the first frame update
    void Start()
    {
        _playerhealth = _player.transform.GetComponent<Health>();
        _currentTime = 0;
        //currentTimeText.text = "Time left: " + _currentTime;
        stopwatchActive = true;

    }

    // Update is called once per frame
    void Update()
    {
     
       
        if (Input.GetKeyDown(KeyCode.Space) && _forTesting == true)
        {
            StopStopwatch();
            _forTesting = false;
        }

        else if (Input.GetKeyDown(KeyCode.O) && _forTesting == false)
        {
            StartStopwatch();
            _forTesting = true;
        }

        if (stopwatchActive == true)
        {
            _currentTime = _currentTime + Time.deltaTime;

            

        }
        if (_playerhealth._health <= 0)
        {
           StopStopwatch();
        }
        TimeSpan time = TimeSpan.FromSeconds(_currentTime);
        currentTimeText.text = time.ToString(@"mm\:ss\:ff");

        

        

         

    }

       

        public void StartStopwatch()
        {
            stopwatchActive = true;
        }

         public void StopStopwatch()
        {
        timeAlive = _currentTime;
        currentTimeText.text = timeAlive.ToString();
        stopwatchActive = false;
          
}
    }

