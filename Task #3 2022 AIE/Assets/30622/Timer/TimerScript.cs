using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
public class TimerScript : MonoBehaviour
{
 [SerializeField] float _currentTime;
[SerializeField] int _startseconds;
[SerializeField] TextMeshProUGUI _currentText;
    public bool timerActive;
    [SerializeField] WeaponSelect _weaponSelect;
    
    [SerializeField] Health _heath;

    // Start is called before the first frame update
    void Start()
    {
       _currentTime = _weaponSelect.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerActive == true)
        {
            _currentTime = _currentTime - Time.deltaTime;
            if (_currentTime <= 0)
            {
                timerActive = false;
                Debug.Log("Timer Finished!");
                Start();
            }
          
        }
          if (_weaponSelect == true)
        {
            StartTimer();
        }
        TimeSpan time = TimeSpan.FromSeconds(_currentTime);
        _currentText.text = "Time until next weapon: " + time.Minutes.ToString() + ":" + time.Seconds.ToString();
    }

     public void StartTimer()
        {
            timerActive = true;
        }

         public void StopTimer()
        {
            timerActive = false;
        }
}
